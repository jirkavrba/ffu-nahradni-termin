(function( $ ) {
	'use strict';

    var esVSEsearch = {
        vars: {
            formSelector: '.es-vse-search__form',
            inputSelector: '.es-vse-search-input',
            autocompleteSelector: '.es-vse-search-autocomplete',
            contextSelector: '.es-vse-search-context',
            crossSelector: '.es-vse-search__cross',
            tabSelector: '.es-vse-search__tab',
            activeTabSelector: '.es-vse-search__tab.active',
            submitSelector: '.es-vse-search__submit',
            menuItemSelector: '.es-vse-autocomplete .ui-menu-item',
            getMoreSelector: '.es-vse-get-more',
            resultsSelector: '.es-vse-results',
            loaderSelector: '.es-vse-loader'
        },
        init: function () {

            esVSEsearch.crossVisibility();
            $(esVSEsearch.vars.getMoreSelector).on('click', function () {
                esVSEsearch.ajaxSearch();
            });

            $(esVSEsearch.vars.autocompleteSelector).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: es_vse_ajax_url.ajaxurl,
                        data: {
                            q: request.term,
                            action: 'es_vse_suggesting',
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) {
                    $(this).val(ui.item.value);
                    $(this).closest('form').submit();
                },
                minLength: 2,
                delay: 200
            });


            $(esVSEsearch.vars.inputSelector).on('keyup', function (event) {
                if (event.keyCode === 13) {
                    esVSEsearch.submitForm();
                }
                esVSEsearch.crossVisibility();

            });


            $(esVSEsearch.vars.crossSelector).on('click', function () {
                $(esVSEsearch.vars.inputSelector).val('');
                esVSEsearch.crossVisibility();
            });

            $(esVSEsearch.vars.tabSelector).on('click', function (e) {
                e.preventDefault();
                if (!($(this).hasClass('active'))) {
                    $(esVSEsearch.vars.tabSelector).removeClass('active');
                    $(this).addClass('active');
                    $(esVSEsearch.vars.contextSelector).val($(this).attr('data-context'));
                    esVSEsearch.submitForm();
                }
            });
            $(esVSEsearch.vars.menuItemSelector).on('click', function () {
                esVSEsearch.submitForm();
            });
            $(esVSEsearch.vars.tabSelector).on('click', function () {
                if (!($(this).hasClass('active'))) {
                    $(esVSEsearch.vars.tabSelector).removeClass('active');
                    $(this).addClass('active');
                    esVSEsearch.submitForm();
                }
            });
        },

        crossVisibility: function () {
            if ($(esVSEsearch.vars.inputSelector).length > 0 && $(esVSEsearch.vars.inputSelector).val()) {
                $(esVSEsearch.vars.crossSelector).css('display','block');
            } else {

                $(esVSEsearch.vars.crossSelector).css('display','none');
            }
        },

        submitForm: function () {
          $(esVSEsearch.vars.formSelector).submit();
        },

        ajaxSearch: function () {
            var context = $(esVSEsearch.vars.contextSelector).val();
            var offset = $(esVSEsearch.vars.formSelector).attr('data-offset');
            var query = $(esVSEsearch.vars.formSelector).attr('data-query');

            $(esVSEsearch.vars.loaderSelector).show();
            $(esVSEsearch.vars.getMoreSelector)[0].disabled = true;
            $.ajax({
                url: es_vse_ajax_url.ajaxurl,
                type: 'POST',
                data: {
                    action: 'es_vse_search',
                    offset: offset,
                    query: query,
                    context: context
                },
                success: function (response){
                    $(esVSEsearch.vars.getMoreSelector)[0].disabled = false ;

                    $(esVSEsearch.vars.loaderSelector).hide();
                    if (response.results.length > 0) {
                        var offset = $(esVSEsearch.vars.formSelector).attr('data-offset');
                        var limit = $(esVSEsearch.vars.formSelector).attr('data-limit');
                        var totalCount = $(esVSEsearch.vars.formSelector).attr('data-total-count');
                        var newOffset = parseInt(offset) + parseInt(limit);
                        $(esVSEsearch.vars.formSelector).attr('data-offset', newOffset);
                        if (newOffset >= totalCount) {
                            $(esVSEsearch.vars.getMoreSelector).hide();
                        }
                        $(esVSEsearch.vars.resultsSelector).append(response.results)
                    }
                },
                error: function () {
                    $(esVSEsearch.vars.getMoreSelector)[0].disabled = false ;
                    $(esVSEsearch.vars.loaderSelector).hide();
                }
            });
        }

    };

    jQuery(document).ready( function(){
        esVSEsearch.init();
    });

})( jQuery );

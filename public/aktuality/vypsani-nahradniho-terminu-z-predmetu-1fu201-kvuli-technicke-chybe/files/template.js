"use strict";

(function ($, root) {
  $(function () {
    //region detekce IE
    if (document.documentMode || /MSIE|Trident/.test(navigator.userAgent)){
      var ieWarning = $('#ieWarning');
      var ieWarningContainer = $('<div class="container" style="font-size:1rem"></div>');
      ieWarningContainer.append($('<div class="font-weight-bold"></div>').html(ieWarning.data('heading')));

      var ieWarningText = $('<div class="container" style="font-size: 0.95rem;margin-top:5px;"></div>');
      ieWarningText.append(ieWarning.data('text'));
      if (ieWarning.data('text-vse')){
        ieWarningText.append(' ',ieWarning.data('text-vse'));
      }

      ieWarning.data('heading','');
      ieWarning.data('text','');

      ieWarning.html(ieWarningContainer);
      ieWarning.removeClass('d-none');
      ieWarning.addClass('alert alert-danger py-1 mb-0 border-0');

      $('body').prepend(ieWarning.clone().append(ieWarningText));

      ieWarning.css({'position':'fixed','top':0,'left':0,'width':'100%','z-index':10000});
    }
    //endregion detekce IE

    //region menu
    /**
     * Metoda pro rozbalení top položky menu
     * @param menuItemId
     */
    function showMenuItem(menuItemId){
      var modalMenuToogleButton=$('#modal-menu-toogle-button');
      if (modalMenuToogleButton.is(':visible')){
        //jde o mobilní menu
        var modalMenu = $('#modal-mainmenu');
        if (!modalMenu.hasClass('show')){
          modalMenuToogleButton.click();
        }
        var modalMenuItem=modalMenu.find('#modal-menu-item-'+menuItemId);
        if (!modalMenuItem.closest('li').hasClass('show')){
          modalMenuItem.click();
        }
      }else{
        //jde o desktopové menu
        var mainMenuItem=$('#main-menu-item-'+menuItemId);
        if (!mainMenuItem.closest('li').hasClass('show')){
          mainMenuItem.click();
        }
      }
    }

    //přiřazení akcí pro drobečkovou navigaci
    $('#breadcrumb').find('a.breadcrumb-main-menu').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      showMenuItem($(this).attr('data-menu-item'));
    });

    //odebrání nadbytečných položek menu
    $('ul.nav li.remove').remove();
    //endregion menu

    //region nastylování tabulek
    $('main table').each(function(){
      var table=$(this);
      if (table.hasClass('styled')){
        //tabulku nemáme nijak upravovat
        return;
      }

      //region přesunutí tabulky do table-responsive-lg kontejneru
      var parentElement=table.parent();
      if (!(parentElement.hasClass('table-responsive')||parentElement.hasClass('table-responsive-xl')||parentElement.hasClass('table-responsive-lg')||parentElement.hasClass('table-responsive-sm')||parentElement.hasClass('table-responsive-md'))){
        var tableContainer=$('<div class="table-responsive-lg"></div>');
        table.before(tableContainer);
        tableContainer.html(table);
      }
      //endregion přesunutí tabulky do table-responsive-lg kontejneru

      //region styly pro tabulku
      if (!table.hasClass('table')){
        table.addClass('table');
        //region pořešení rámečků
        if (table.attr('border')>0){
          table.addClass('table-bordered');
        }
        //endregion pořešení rámečků

        if (!table.children('thead').length){
          //tabulka nemá záhlaví
          var thead=$('<thead></thead>');
          var firstRow=true;
          var isHead=true;
          var prependThead=false;
          table.find('tr').each(function(){
            //procházíme jednotlivé řádky
            var tr=$(this);
            if (firstRow){
              if (tr.children('th').length===0){
                //tabulka nemá určené žádné buňky záhlaví
                var trHtml=tr.html();
                trHtml=trHtml.replace(/<td/g,'<th').replace(/<\/td/g,'</th');
                tr.html(trHtml);
                table.prepend(thead);
                thead.append(tr);
                isHead=false;
              }else if(tr.children('td').length>0){
                isHead=false;
              }else {
                table.prepend(thead);
                thead.append(tr);
              }
              firstRow=false;
              return;
            }
            if (isHead){
              if (tr.children('td').length===0 && tr.children('th').length>0){
                thead.append(tr);
              }else {
                isHead=false;
              }
            }
          });
        }
      }
      //endregion styly pro tabulku
    });
    //endregion nastylování tabulek

    //region modální okno vyhledávání
    $('#modal-search').on('shown.bs.modal', function(){
      var searchInput=$('#modal-search-input');
      searchInput.select();
      searchInput.focus();
    });
    //endregion modální okno vyhledávání

    //region collapsable
    $('.collapse-more').each(function(){
      var content = $(this);
      var contentId=content.attr('id');
      if (!contentId){
        contentId='collapse-more-rand-'.Math.random().toString(36);
        content.attr('id',contentId);
      }
      
      var btn = $('<button type="button" role="button" aria-expanded="false"></button>');
      btn.attr('class',(content.data('button-class')?content.data('button-class'):'btn btn-primary'));
      btn.attr('aria-controls',contentId);
      btn.attr('id',contentId+'-expand-button');
      btn.text((content.data('button')?content.data('button'):'...'));

      var btnDiv = $('<div class="my-2 text-center border-1 border-primary"></div>');
      btnDiv.append(btn);

      btn.click(function(e){
        e.preventDefault();
        e.stopPropagation();
        content.collapse('show');
        btnDiv.remove();
        if (content.attr('data-linkable-id')){
          location.hash='#'+contentId;
        }
      });

      content.removeClass('collapse-more');
      content.addClass('collapse');
      content.attr('aria-expanded',false);
      content.attr('aria-controls',contentId);
      content.after(btnDiv);
    });
    //endregion collapse
    //region rozbalení sekce podle jejího ID
    var expandSectionByHash = function(){
      if (location.hash){
        var element = $(location.hash);
        if (element.hasClass('collapse-section')){
          if (element.hasClass('collapse')){
            element.parent().find('button#' + element.attr('id') + '-expand-button').click();
          }
        }else if(element.hasClass('collapse-card')){
          var collapseCardExpandLink = element.find('a.collapse-card-expand-link');
          if (collapseCardExpandLink.hasClass('collapsed')){
            collapseCardExpandLink.click();
          }
        }
      }
    };

    $(window).on('hashchange',function(e){
      expandSectionByHash();
    });
    expandSectionByHash();
    //endregion rozbalení sekce podle jejího ID
  });
})(jQuery, this);